#!/bin/zsh
#
# Sets up requirements to provision with ansible
#

#
# Clean display function
#
# usage:
#        display "My thing to output"
#

########################
### Helper Functions ###
########################
function display() {
  echo "----> $1"
}

########################
##### Shell Setup ######
########################
display "ensuring zsh is set to default shell"
chsh -s /bin/zsh

# TODO:
display "ensuring oh-my-zsh is installed"
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

########################
##### Python Setup #####
########################
if [ ! `which pip` ]
then
  display "pip is not installed"
  display "installing pip"
  sudo easy_install pip
else
  display "pip is already installed"
fi

sudo pip install --upgrade -r requirements.txt --ignore-installed six

########################
#### Homebrew Setup ####
########################
if [ ! `which brew` ]
then
  display "installing homebrew"
  /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
else
  display "homebrew is already installed"
fi
