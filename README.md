Personal Ansible Setup

# Getting Started

1. The first step to running these setup scripts is to pull this repo down into your hom directory. To do this, you will need to have the MacOSX CLI tools installed (git). You will get prompted by OSX to install them if you run any git command `git`
1. You are now able to kick everything of by running `make` inside of the `my-ansible` project.

# TODO: Add to setup

Note: Most of the System Preferences are taken care of by .macos, but you should
      verify the following. (Soo... .macos needs to be tweeked a bit for Sierra)
1. System Preferences
   - Trackpad
     - Point & Click
       - Tap to click = true
     - Scroll & Zoom
       - Scroll Direction: Natural = false
   - Keyboard
     - Keyboard
       - Key Repeat = fast
       - Delay Until Repeat = short
     - Shortcuts
       - Mission Control = all off (messes with emacs)
     - Modify Keys
       - Remap caps lock -> Control
1. Blender4web (no cask.. yet)
1. Datomic
   - Install Datomic Pro from https://my.datomic.com/downloads/pro to `~/datomic/datomic-{version#}`
   - Copy the dev transactor from `config/samples` to `config` and add the liscense key
